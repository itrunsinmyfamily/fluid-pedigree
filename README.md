# ItRuns Fluid Pedigree

# KNOWN BUGS

  1. When parental generation is small (only mother + father, for instance) the grandparents overlap one another. We need
     some mechanism for adding buffer space based on parental size?
  2. We're using descendents to create buffer space, which generally works but it ends up adding space in our grandparents
     generation also. For instance, this produces incorrect gaps between my grandmother and her siblings. 
  3. Father + Mother in single parent group don't have a spousal relationship. This means they have a divorce. We need to
     add an edge.
  4. Crooked edges on single parents. Extra confusing when there is no vertex because it doubles back.
  5. Sometimes there's a patient directly below the vertex and the patient then is displaced out of its leaf row
     a bit. I suspect this is due to slack in the link distance. Try tightening link distances, possibly conditionally?
  6. Sometimes on reload all the nodes get stuck in the middle without any positions. It's some race condition between d3 and 
     webcola, because if you touch the graph it will suddenly snap into place.
  7. It's non-ideal that newly entering or leaving nodes don't transition in/out. It would be nice if, when I'm adding
     somebody, the graph makes room with a transition, not suddenly.
  8. Changing between presets often sees a kind of big zoomfit. Seems like elements should start more centered. Maybe zoom needs to be more correctly reset between graphs.

## Release Instructions

It is important to properly version releases to ensure frontend and backend dependencies stay in sync. Use the
following process,

  1. Rebuild the bundle: `npm run build`
  2. Bump the package version: `npm version <something>` where 'something' depends on the severity of your change. For example, `patch`
  3. Update bower.json to match your version from step #2. Commit this change.
  4. Push your changes, including tags, to bitbucket: `git push --follow-tags`
  5. In the frontend project, upgrade the bower dependency for fluid-pedigree if/when necessary.
