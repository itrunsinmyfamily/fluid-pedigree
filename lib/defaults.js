export default {
  margin: { top: 0, right: 0, bottom: 0, left: 0 },

  pedigree: {
    spriteSize: 120,
    patientNodeSize: 60,
    familyChildGap: 90,

    sortingKey: 'age',

    showEdges: true,
    showVertexes: true,
    showLinks: false,
  },
}
