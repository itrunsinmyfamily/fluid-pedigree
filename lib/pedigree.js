/* eslint no-magic-numbers: "off", no-console: "off", id-match: "off", no-param-reassign: "off" */
import { SvgChart, helper } from 'd3kit'
import * as cola from 'webcola'
import template from 'lodash.template'

// This is a scoped import of only the d3 modules we need for Cola
import * as d3context from './d3-context'
import defaults from './defaults'

import Graph from './graph'
import { edgePathGenerator } from './utils'

export default class FluidPedigree extends SvgChart {
  static getDefaultOptions() {
    return helper.deepExtend(super.getDefaultOptions(), defaults)
  }

  static getCustomEventNames() {
    return ['changed', 'selected', 'hovered', 'highlighted', 'dragstart', 'dragend', 'dragged', 'contextmenu']
  }

  constructor(container, options) {
    super(container, options)

    options = this.options()

    this.zoom = d3context.zoom()
    this.chartRoot.call(this.zoom.on('zoom', () => {
      const transform = d3context.event.transform

      this.rootG.style('transform', `translate(${transform.x}px, ${transform.y}px) scale(${transform.k})`)
    }))

    this.on('data.default', data => {
      this.graph.update(data)
      this.render()
    })

    this.on('options.default', () => this.render())
    this.on('changed.default', () => this.updateExtents())

    // Compile all templates
    this.templates = {}
    Object.keys(this.options().templates || {}).forEach(key => {
      this.templates[key] = template(this.options().templates[key])
    })

    // Append SVG defs (filters, etc)
    this.rootG.append('defs').html(() => this.templates.defs())
    this.layers.create(['lines', 'shapes'])

    this.init()
  }

  init() {
    this.selected = null
    this.highlighted = null
    this.restarted = true
    this.graph = new Graph(this.options().pedigree)
    this.graph.maxStep = Infinity

    this.simulation = cola.d3adaptor(d3context)
      .handleDisconnected(false)
      .linkDistance(60)
      .avoidOverlaps(false)

    this.layers.get('shapes').selectAll('*').remove()
    this.layers.get('lines').selectAll('*').remove()

    this.resize()
  }

  resize(targetSize = [], zoomDuration = 0) {
    const parent = this.chartRoot.node()
    const size = this.simulation.size()

    targetSize[0] = targetSize[0] || parent.clientWidth
    targetSize[1] = targetSize[1] || parent.clientHeight

    if (size[0] !== targetSize[0] || size[0] !== targetSize[1]) {
      this.simulation.size(targetSize)

      this.updateExtents(targetSize)
      this.zoomFit(zoomDuration, targetSize)
    }
  }

  updateExtents(targetSize) {
    const fit = this.calculateFit(targetSize)

    if (!fit) {
      return
    }

    let minScale = fit.scale * 0.66

    if (minScale > 1) {
      minScale = 1.0
    }

    // Allow full zooming out to beyond 1.5x the natural size of the graph, but zooming in limited to 2x fixed
    this.zoom.scaleExtent([minScale, 2])
  }

  render() {
    if (!this.hasData() || !this.graph.nodes.length) {
      return
    }

    const options = this.options()
    const spriteSize = options.pedigree.spriteSize

    const restarted = this.restarted
    const changed = this.graph.changed
    const rearranged = this.graph.rearranged

    if (restarted || changed) {
      this.simulation
        .nodes(this.graph.nodes)
        .links(this.graph.links)
        .constraints(this.graph.constraints)

      if (restarted) {
        this.simulation.start(10, 50, 10)
        this.restarted = false
      } else {
        this.simulation.start()
      }

      this.graph.changed = false
    }

    const shapes = this.layers.get('shapes')
    const lines = this.layers.get('lines')

    this.container.classed('highlighted', this.highlighted)

    // Selections of all elements by class (entering, updating, and exiting)
    const all = {
      links: lines.selectAll('.fluid-pedigree-link'),
      edges: lines.selectAll('.fluid-pedigree-family-edge'),
      patients: shapes.selectAll('.fluid-pedigree-patient'),
      vertexes: shapes.selectAll('.fluid-pedigree-family-vertex'),
    }

    // Selections of elements that were already present (non-entering, non-exiting)
    const updating = {
      links: all.links.data(this.graph.links, d => d.id),
      edges: all.edges.data(this.graph.edges, d => d.id),
      patients: all.patients.data(this.graph.patients, d => d.id),
      vertexes: all.vertexes.data(this.graph.vertexes, d => d.family.id),
    }

    // Selections of elements entering the graph
    const entering = {
      links: updating.links.enter().append('line'),
      edges: updating.edges.enter().append('path'),
      patients: updating.patients.enter().append('svg'),
      vertexes: updating.vertexes.enter().append('svg'),
    }

    // Selections of elements present and active in the graph (Union of updating and entering elements)
    const present = {
      links: entering.links.merge(updating.links),
      edges: entering.edges.merge(updating.edges),
      patients: entering.patients.merge(updating.patients),
      vertexes: entering.vertexes.merge(updating.vertexes),
    }

    // Selections of elements exiting the graph
    const exiting = {
      links: updating.links.exit(),
      edges: updating.edges.exit(),
      patients: updating.patients.exit(),
      vertexes: updating.vertexes.exit(),
    }

    updating.patients.html(d =>
      this.templates.patient({
        patient: d,
        entering: false,
        selected: (d.id === this.selected),
        options: options.pedigree,
      }))

    entering.links.attr('class', 'fluid-pedigree-link')
    entering.edges.attr('class', 'fluid-pedigree-family-edge')
    entering.patients
      .attr('id', d => `patient-sprite-${d.id}`)
      .attr('width', spriteSize)
      .attr('height', spriteSize)
      .attr('class', 'fluid-pedigree-patient')
      .attr('viewBox', `-${spriteSize / 2} -${spriteSize / 2} ${spriteSize} ${spriteSize}`)
      .on('click', d => (this.selected !== d.id ? this.select(d.id) : this.select(null)))
      .on('mouseover', d => this.dispatcher.call('hovered', this, d, d3context.event))
      .on('mouseout', () => this.dispatcher.call('hovered', this, null, d3context.event))
      .on('contextmenu', d => this.dispatcher.call('contextmenu', this, d, d3context.event))
      .html(d =>
        this.templates.patient({ patient: d,
          entering: true,
          selected: (d.id === this.selected),
          options: options.pedigree,
        }))

    entering.vertexes
      .attr('class', 'fluid-pedigree-family-vertex')
      .attr('width', spriteSize)
      .attr('height', spriteSize)
      .attr('viewBox', `-${spriteSize / 2} -${spriteSize / 2} ${spriteSize} ${spriteSize}`)
      .on('click', v => {
        this.graph.collapseFamily(v.family.id)
        this.render()
      })

    present.links.style('display', options.pedigree.showLinks ? 'initial' : 'none')
    present.edges.classed('adopted', d => d.type === 'child' && d.target.adopted)

    present.patients
      .classed('selected', d => d.id === this.selected)
      .classed('filtered', d => this.highlighted && !this.highlighted.includes(d.id))
      .classed('highlighted', d => this.highlighted && this.highlighted.includes(d.id))
      .call(this.simulation.drag()
        .on('drag', d => this.dispatcher.call('dragged', this, d, d3context.event)))

    present.vertexes
      .style('display', d => (d.family.collapsed || d.family.children.length > 1 ? 'initial' : 'none'))
      .html(d => this.templates.familyVertex({ v: d, options: options.pedigree }))
      .call(this.simulation.drag)

    // Exiting elements should be removed
    exiting.links.remove()
    exiting.edges.remove()
    exiting.vertexes.remove()
    // Exiting patients should be removed after a small transition
    exiting.patients.transition()
            .ease(d3context.easeExpIn)
            .duration(300)
            .attr('width', 0)
            .attr('height', 0)
            .attr('x', d => d.x)
            .attr('y', d => d.y)
            .remove()

    // Apply transition to provided selections. Used to smooth abrupt
    // transitions on update of existing nodes on simulation restart, which often happens when
    // node data was changed causing the node's placement based on constraints to suddenly move
    // substantially.
    const transitioning = set => ({
      links: set.links.transition().duration(500),
      edges: set.edges.transition().duration(500),
      patients: set.patients.transition().duration(500),
      vertexes: set.vertexes.transition().duration(500),
    })

    let enableTransition = rearranged

    this.simulation.on('tick', () => {
      this.tick(enableTransition ? transitioning(present) : present)
    })
    .on('end', () => {
      // Clear all transitions after initial simulation stabilizes
      enableTransition = false
    })

    // Perform a single tick immediately to ensure any fitting behavior has at least seed positions
    this.simulation.tick()

    if (restarted) {
      this.zoomFit()
    }
    if (changed) {
      this.dispatcher.call('changed', this)
    }
  }

  tick(selections) {
    const { vertexes, edges, links, patients } = selections
    const spriteRadius = this.options().pedigree.spriteSize / 2

    vertexes.attr('x', d => d.x - spriteRadius)
            .attr('y', d => d.y - spriteRadius)

    edges.attr('d', edgePathGenerator)

    links.attr('x1', d => d.source.x)
         .attr('y1', d => d.source.y)
         .attr('x2', d => d.target.x)
         .attr('y2', d => d.target.y)

    patients.attr('x', d => d.x - spriteRadius)
            .attr('y', d => d.y - spriteRadius)
  }

  select(id, skipDispatch = false, skipRender = false) {
    const selectedPatient = (id === null) ? null : this.findPatient(id)

    if (selectedPatient === undefined) {
      return
    }

    const selected = selectedPatient ? String(selectedPatient.id) : null
    const changed = this.selected !== selected

    if (changed) {
      this.selected = selected
      if (!skipDispatch) {
        this.dispatcher.call('selected', this, selectedPatient)
      }

      if (!skipRender) {
        this.render()
      }
    }
  }

  highlight(highlighted = null) {
    let patients = null

    if (highlighted) {
      highlighted = highlighted.map(String)
      patients = highlighted.map(id => this.findPatient(id))
    }

    const changed = this.highlighted !== highlighted

    if (changed) {
      this.highlighted = highlighted
      this.dispatcher.call('highlighted', this, patients)
      this.render()
    }
  }

  selectedPatient() {
    if (this.selected) {
      return this.findPatient(this.selected)
    }

    return null
  }

  findPatient(id) {
    return this.graph.findPatient(id)
  }

  calculateFit(targetSize, padding = 0, maxScale = Infinity) {
    const bounds = this.rootG.node().getBBox()

    const width = bounds.width
    const height = bounds.height
    const midX = bounds.x + (width / 2)
    const midY = bounds.y + (height / 2)

    if (width === 0 || height === 0) {
      return null
    }

    if (!targetSize) {
      const parent = this.chartRoot.node()

      targetSize = [parent.clientWidth, parent.clientHeight]
    }

    let scale = (1 - padding) / Math.max(width / targetSize[0], height / targetSize[1])

    if (scale > maxScale) {
      scale = maxScale
    }

    const translate = [(targetSize[0] / 2) - (scale * midX), (targetSize[1] / 2) - (scale * midY)]

    return { scale, translate }
  }

  zoomFit(duration = 0, targetSize, padding = 0.2) {
    const fit = this.calculateFit(targetSize, padding, 1.0)

    if (!fit) {
      return Promise.resolve()
    }

    const transition = this.chartRoot.transition()
      .duration(duration)
      .call(this.zoom.transform, d3context.zoomIdentity.translate(...fit.translate).scale(fit.scale))

    return new Promise(resolve => {
      transition.on('end', resolve)
    })
  }
}
