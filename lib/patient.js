export default class Patient {
  constructor(id) {
    this.id = id
    this.gender = null
    this.age = null
    this.proband = false
    this.ancestor = false
    this.twinId = undefined
    this.relations = { parent: [], child: [], spousal: [] }

    this.family = null
    this.group = null
  }

  isOrphaned() {
    return !this.relations.parent.length
  }

  getFamilyId() {
    if (this.isOrphaned()) {
      return `orphaned@${this.id}`
    }

    return this.relations.parent.map(p => p.id).sort().join(':')
  }
}
