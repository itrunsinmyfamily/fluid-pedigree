import { cmpFamilies } from './utils'

let PARENTAL_GROUP_INDEX = 0

export default class ParentalGroup {
  constructor(options) {
    this.options = options
    this.id = 'pg' + PARENTAL_GROUP_INDEX++
    this.families = []
  }

  static fromFamily(family, options) {
    const parentWithGroup = family.parents.find(p => p.group)

    if (parentWithGroup) {
      return parentWithGroup.group
    }

    return new ParentalGroup(options)
  }

  addFamily(family) {
    this.families.push(family)
  }

  rebuild() {
    this.members = []
    this.vertexes = []
    this.leaves = []
    this.links = []
    this.edges = []
    this.constraints = []

    // Organize families based on family composition
    this.families.sort(cmpFamilies)

    // Within each family, organize the children based on many factors. Then merge those children and their
    // spouses into our group's overall pool of children.
    this.families.forEach(f => {
      f.rebuild()

      this.members.push(...f.members)
      this.leaves.push(...f.leaves)
      this.links.push(...f.links)
      this.edges.push(...f.edges)
      this.constraints.push(...f.constraints)

      if (f.vertex) {
        this.vertexes.push(f.vertex)
      }
    })

    let prev = null
    const familyChildGap = this.options.familyChildGap

    this.leaves.forEach(l => {
      if (prev) {
        const spousal = l.relations && l.relations.spousal.find(s => s === prev)
        const descendents = (prev.descendents || 0) + (l.descendents || 0)
        let gap = familyChildGap

        if (spousal) {
          // Modest gap increase for spouses
          // eslint-disable-next-line no-magic-numbers
          gap *= 1.5
        } else {
          if (l.family !== prev.family) {
            // Notable gap between families within a parent group
            gap *= 2
          }

          if (descendents > 1 && !l.collapsed && !prev.collapsed) {
            // We inject additional space between siblings in scaling proportion to the number of descendents
            // NOTE: This logic is flawed, as very deep family trees have different shape than shallow broad ones
            // eslint-disable-next-line no-magic-numbers
            gap += familyChildGap * ((descendents - 1) / 4)
          }
        }

        this.constraints.push({ axis: 'x', left: prev, right: l, gap })
      }

      prev = l
    })
  }
}
