// eslint-disable-next-line id-match
import * as d3context from './d3-context'
import ParentalGroup from './parental-group'
import { cmpSiblings, disoriented } from './utils'

export default class Family {
  constructor(familyId, parents, options, vertex = {}, collapsed = false, orphaned = false) {
    this.id = familyId
    this.parents = Object.assign([], parents)
    this.children = []
    this.collapsed = collapsed ? [] : undefined
    this.step = Math.min(...parents.filter(p => p.step !== undefined).map(p => p.step))
    this.orphaned = orphaned
    this.options = options

    if (!orphaned) {
      // Orphaned familes have no parents and no vertex
      this.vertex = vertex
      this.vertex.family = this
      this.vertex.collapsed = this.collapsed
    }

    this.group = ParentalGroup.fromFamily(this, options)
    // Ensure all parents are members of the group
    this.group.addFamily(this)
    this.parents.forEach(p => (p.group = this.group))
  }

  static fromPatient(patient, options, vertex, collapsed) {
    return new Family(patient.getFamilyId(), patient.relations.parent, options, vertex, collapsed, patient.isOrphaned())
  }

  add(patient) {
    if (this.step === undefined || this.step > patient.step) {
      this.step = patient.step
    }

    if (this.collapsed && !patient.ancestor) {
      this.collapsed.push(patient)
      return
    }

    this.children.push(patient)

    patient.family = this
  }

  rebuild() {
    const patientNodeSize = this.options.patientNodeSize
    const ancestor = this.children.find(p => p.ancestor)
    const leafyVertex = this.collapsed || (this.children.length === 1 && ancestor)


    // Spouses of children
    this.spouses = []
    // Children and their spouses, sorted left to right
    this.leaves = []
    // Parents and all leaves
    this.members = [...this.parents]

    this.links = []
    this.edges = []
    this.constraints = []

    this.children.sort(cmpSiblings)

    this.children.forEach(c => {
      const spouses = c.relations.spousal.filter(s => !this.collapsed || s.ancestor)

      if (c.gender === 'male') {
        this.leaves.push(c, ...spouses)
      } else {
        this.leaves.push(...spouses, c)
      }

      this.spouses.push(...spouses)
    })

    this.members.push(...this.children, ...this.spouses)

    if (this.orphaned) {
      // Orphaned familes have no parents and no vertex
      return
    }

    if (leafyVertex) {
      if (ancestor && ancestor.gender === 'male') {
        this.leaves.unshift(this.vertex)
      } else {
        this.leaves.push(this.vertex)
      }
    }

    this.parents.forEach(p => {
      const gap = leafyVertex ? (2 * patientNodeSize) : patientNodeSize

      this.links.push({ id: `pv${this.id}:${p.id}`, source: p, target: this.vertex })
      this.constraints.push({ axis: 'y', left: p, right: this.vertex, gap })
    })

    // We form an edge between the parents and the vertex
    this.edges.push({ id: `ep${this.id}`, source: this.parents, target: this.vertex, type: 'parent' })

    if (this.parents.length > 1 && !this.parents[0].relations.spousal.includes(this.parents[1])) {
      this.edges.push({ id: `ep${this.id}`, source: this.parents[0], target: this.parents[1], type: 'divorce' })
    }

    this.children.forEach(c => {
      this.links.push({ id: `vc${this.id}:${c.id}`, source: this.vertex, target: c })
      this.edges.push({ id: `ec${this.id}:${c.id}`, source: this.vertex, target: c, type: 'child' })

      if (c.twinId !== undefined) {
        c.twinset = this.children.filter(n => n.twinId === c.twinId)
      }
    })

    if (leafyVertex) {
      // If collapsed, the vertex is part of the leaves. Since the links no longer encourage y-axis alignment
      // we lock the y-axis of the leaves.

      this.constraints.push({ type: 'alignment', axis: 'y', offsets: this.leaves.map(node => ({ node, offset: 0 })) })
    } else {
      this.leaves.filter(l => l !== this.vertex).forEach(l => {
        this.constraints.push({ axis: 'y', left: this.vertex, right: l, gap: patientNodeSize })
      })

      // Vertex must remain within the bounds of the leaves
      this.constraints.push({ axis: 'x', left: this.leaves[0], right: this.vertex, gap: 0 })
      this.constraints.push({ axis: 'x', left: this.vertex, right: this.leaves[this.leaves.length - 1], gap: 0 })
    }
    this.seed()
  }

  seed() {
    const patientNodeSize = this.options.patientNodeSize
    const all = [...this.parents, ...this.children, ...this.spouses]

    // Start by initializing the vertex's position based on any existing family positional data
    if (disoriented(this.vertex)) {
      this.vertex.x = d3context.mean(all, n => n.x)

      const py = d3context.max(this.parents, d => d.y)
      const cy = d3context.min(this.children, d => d.y)

      if (py !== undefined && cy !== undefined) {
        this.vertex.y = (py + cy) / 2
      } else if (py !== undefined) {
        this.vertex.y = py + patientNodeSize
      } else {
        this.vertex.y = cy - patientNodeSize
      }

      // console.log('Seeded Vertex:', this.vertex.x + ',' + this.vertex.y)
    }

    // Next initialize any patients in the family at the position of the vertex
    all.filter(disoriented).forEach(n => {
      n.x = this.vertex.x
      n.y = this.vertex.y
      // console.log('Seeded Patient', n.id + ':', n.x + ',' + n.y)
    })
  }
}
