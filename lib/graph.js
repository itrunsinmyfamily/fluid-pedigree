import Family from './family'
import Patient from './patient'

export default class Graph {
  constructor(options) {
    this.options = options

    this.changed = false
    this.rearranged = false
    this.maxStep = Infinity

    this.graph = {}
    this.nodes = []
    this.patients = []
    this.families = []
    this.vertexes = []
    this.links = []
    this.edges = []
    this.groups = []
    this.constraints = []
    this.collapsed = {}
  }

  validate(data) {
    const patients = Object.keys(data).map(id => data[id])
    const proband = patients.find(p => p.proband)

    if (!proband) {
      throw new Error('Invalid Graph: Missing proband node!')
    }
  }

  update(data) {
    if (data) {
      this.validate(data)
      this.mergeGraphData(data)
    }

    this.buildFamilies()
    this.buildGraph()
  }

  buildFamilies() {
    this.clearNodes()
    this.families = []
    this.groups = []
    const families = {}
    const vertexes = {}

    // Save for reuse
    this.vertexes.forEach(v => (vertexes[v.family.id] = v))

    const walkDescendents = patient => {
      if (patient.descendents === undefined) {
        patient.descendents = patient.relations.child.length
        patient.relations.child.forEach(c => {
          patient.descendents += walkDescendents(c)
        })

        // It's important that our spouses also walk their descendents, and often we are their only
        // link to the graph.
        patient.relations.spousal.forEach(walkDescendents)
      }

      return patient.descendents
    }

    const walkAncestors = patient => {
      patient.ancestor = true
      patient.relations.parent.forEach(walkAncestors)

      if (patient.relations.parent.length === 0) {
        // From the tops of our graph, walk down all children counting descendents and cascading up results
        walkDescendents(patient)
      }
    }

    this.proband.relations.parent.forEach(walkAncestors)

    // Step through each patient, cataloging minimum step which is necessary for later queuing
    const stepPatients = (patient, step = 0) => {
      if (!('step' in patient) || patient.step > step) {
        patient.step = step

        patient.relations.parent.forEach(n => stepPatients(n, step + 1))
        patient.relations.child.forEach(n => stepPatients(n, step + 1))
      }
    }

    stepPatients(this.proband)

    // Build family membership
    Object.keys(this.graph).forEach(id => {
      const patient = this.graph[id]
      const familyId = patient.getFamilyId()

      if (!families[familyId]) {
        families[familyId] = Family.fromPatient(patient, this.options, vertexes[familyId], this.collapsed[familyId])
      }

      families[familyId].add(patient)
    })

    const walkedFamilies = new Set()
    const walkedPatients = new Set()
    const walkedGroups = new Set()
    const walkFamilies = patient => {
      if (walkedPatients.has(patient)) return
      walkedPatients.add(patient)

      const family = families[patient.getFamilyId()]

      walkedFamilies.add(family)
      walkedGroups.add(family.group)

      patient.relations.parent.forEach(walkFamilies)

      if (!family.collapsed || patient.ancestor) {
        patient.relations.child.forEach(walkFamilies)
      }
    }

    walkFamilies(this.proband)

    this.families = Array.from(walkedFamilies)
    this.groups = Array.from(walkedGroups)
  }

  buildGraph() {
    this.changed = true
    this.nodes.length = 0
    this.patients.length = 0
    this.vertexes.length = 0
    this.links.length = 0
    this.edges.length = 0
    this.constraints.length = 0

    const patientSet = new Set()
    const spousalLinks = {}

    if (!this.proband) {
      return
    }

    if (this.proband.x === undefined || this.proband.y === undefined) {
      // console.log('Seeding proband at 0,0')
      this.proband.x = 0
      this.proband.y = 0
    }

    this.groups.forEach(group => {
      group.rebuild()

      // To prevent duplicates, as patients can exist in multiple families
      group.members.forEach(p => patientSet.add(p))

      this.links.push(...group.links)
      this.edges.push(...group.edges)
      this.constraints.push(...group.constraints)

      group.vertexes.forEach(v => {
        this.addNode(v)
        this.vertexes.push(v)
      })
    })

    patientSet.forEach(p => {
      this.addNode(p)
      this.patients.push(p)

      p.relations.spousal.filter(s => patientSet.has(s)).forEach(s => {
        const spousalPair = [p, s].sort((a, b) => a.id.localeCompare(b.id))
        const id = 'ss' + spousalPair.map(n => n.id).join(':')

        if (!spousalLinks[id]) {
          spousalLinks[id] = { id, source: spousalPair[0], target: spousalPair[1], edge: 'spouse' }
          this.links.push(spousalLinks[id])
          this.edges.push({ id: `s${p.id}:${s.id}`, source: p, target: s, type: 'spousal' })
          this.constraints.push({
            axis: 'y',
            type: 'alignment',
            offsets: [{ node: p, offset: 0 }, { node: s, offset: 0 }],
          })
        }
      })
    })

    // Because cola does not work with object references we now must deref into node indexes.
    // We could not do this before because nodes were present in constraints that were not yet
    // in the node list.
    this.constraints.forEach(c => {
      if (c.type === 'alignment') {
        c.offsets = c.offsets.map(o => ({ node: o.node.index, offset: o.offset }))
      } else {
        c.left = c.left.index
        c.right = c.right.index
      }
    })
  }

  clearNodes() {
    Object.keys(this.graph).forEach(id => {
      const n = this.graph[id]

      delete n.step
      delete n.family
      delete n.group
      delete n.descendents
      delete n.twinset
    })
  }

  addNode(n) {
    n.index = this.nodes.length
    n.size = this.options.patientNodeSize
    this.nodes.push(n)
  }

  stepTo(step) {
    this.maxStep = step >= 0 ? step : 0
    this.buildGraph()
  }

  nextStep() { this.stepTo(this.maxStep + 1) }
  prevStep() { this.stepTo(this.maxStep - 1) }

  collapseFamily(familyId, collapse = !this.collapsed[familyId]) {
    this.collapsed[familyId] = collapse

    if (!collapse) {
      // Clear positions of collapsed children to force re-seeding of positions on reentry
      this.families.find(f => f.id === familyId).collapsed.forEach(c => {
        delete c.x
        delete c.y
      })
    }

    this.update()
  }

  findPatient(id) {
    return this.graph[id]
  }

  mergeGraphData(data) {
    const result = {}
    const EmptyPatient = new Patient()
    const sortingKey = this.options.sortingKey

    this.rearranged = false

    // Shallow merge on top of any existing node
    Object.keys(data).forEach(id => {
      const nodePresent = !!this.graph[id]
      const sortingVal = this.graph[id] && this.graph[id][sortingKey]

      // Reuse graph objects, but ensure any properties not set in data are initialized to defaults with EmptyPatient
      result[id] = Object.assign(this.graph[id] || new Patient(), EmptyPatient, data[id], { id })

      this.rearranged = this.rearranged || !!(nodePresent && result[id][sortingKey] !== sortingVal)
    })

    // Rebuild relations, disregarding unnecessary types and dereferencing IDs
    Object.keys(result).forEach(id => {
      const p = result[id]

      // We only care about parent, child, and spouse relations
      p.relations = {
        parent: (p.relations.parent || []).map(i => result[i]),
        child: (p.relations.child || []).map(i => result[i]),
        spousal: (p.relations.spousal || []).map(i => result[i]),
      }

      if (p.proband) {
        this.proband = p
      }
    })

    this.graph = result

    return this.graph
  }
}
