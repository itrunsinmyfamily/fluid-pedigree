import { line, curveStepAfter } from 'd3-shape'
import * as d3context from './d3-context'

const GENDERBIAS = { male: 1, female: -1 }

export function cmpGender(a, b) {
  const av = (a && a.gender) ? GENDERBIAS[a.gender] : 0
  const bv = (b && b.gender) ? GENDERBIAS[b.gender] : 0

  return bv - av
}


export function cmpSiblings(a, b) {
  // First sort based on ancestry status. Ancestors get pushed to the front or back depending on gender.
  if (a.ancestor && !b.ancestor) {
    return GENDERBIAS[a.gender]
  } else if (b.ancestor && !a.ancestor) {
    return -1 * GENDERBIAS[b.gender]
  }
  // Next sort by age
  const aAge = a.age || 0
  const bAge = b.age || 0

  if (aAge !== bAge) {
    return bAge - aAge
  }

  // Next sort by marriage status. This ensures that married twins get separated out from non-married
  const aHasSpouses = !!a.relations.spousal.length
  const bHasSpouses = !!b.relations.spousal.length

  if (aHasSpouses !== bHasSpouses) {
    // Twins who are married should be sorted in a way that ensures a normal male<->female spousal
    // alignment won't divide twin siblings. So it's gender-based, but inverted - male siblings who are married
    // should be sorted to the right, female married siblings to the left.
    if (aHasSpouses) {
      return GENDERBIAS[a.gender]
    }

    return -1 * GENDERBIAS[b.gender]
  }

  if (aHasSpouses && b.aHasSpouses) {
    // On the other hand, if they both have spouses then we'll do a full inverted gender sort. This
    // will ensure mixed-gender twins get sorted in a way that allow them to both have normally aligned spouses
    return -1 * cmpGender(a, b)
  }

  // If they are both spouseless they are basically identical twins. We sort by ID to ensure a stable sort.
  return a.id.localeCompare(b.id)
}

function findAncestor(f) {
  if (f.children.length === 0) {
    return null
  }

  const first = f.children[0]
  const last = f.children[f.children.length - 1]

  if (first.ancestor) {
    return first
  } else if (last.ancestor) {
    return last
  }

  return null
}

export function cmpFamilies(a, b) {
  // Families with an ancestor get sorted according to gender bias. We assume that our input
  // families have already been sorted, and thus only check the ends of the leaves
  const aAncestor = findAncestor(a)
  const bAncestor = findAncestor(b)

  if (aAncestor && !bAncestor) {
    return GENDERBIAS[aAncestor.gender]
  } else if (bAncestor && !aAncestor) {
    return GENDERBIAS[bAncestor.gender]
  } else if (aAncestor && bAncestor && aAncestor.gender !== bAncestor.gender) {
    return -1 * cmpGender(aAncestor, bAncestor)
  }

  // Families with a single parent get sorted left/right based on gender bias of the parent
  const aSingleParent = a.parents.length === 1
  const bSingleParent = b.parents.length === 1

  if (aSingleParent && !bSingleParent) {
    return GENDERBIAS[a.parents[0].gender]
  } else if (bSingleParent && !aSingleParent) {
    return GENDERBIAS[b.parents[0].gender]
  }

  // Otherwise families are sorted based on the age of the firstborn child
  const aEldest = (a.children[0] && a.children[0].age) || 0
  const bEldest = (a.children[0] && a.children[0].age) || 0

  return aEldest - bEldest
}

const midpoint = d => ({ x: d3context.mean(d, n => n.x), y: d3context.mean(d, n => n.y) })

const basicPathGenerator = line()
const linePathGenerator = line().x(d => d.x).y(d => d.y)
const stepPathGenerator = line().x(d => d.x).y(d => d.y).curve(curveStepAfter)

export function edgePathGenerator(edge) {
  if (edge.type === 'parent') {
    if (edge.source.length > 1) {
      return stepPathGenerator([edge.target, midpoint(edge.source)])
    }

    const target = edge.target
    const source = edge.source[0]
    // These midpoint adjustments are to ensure there are no overlaps when edges need to cross over one another.
    // However, it's also causing dissymmetry in parent groups in which that is not happening. Probably we need to
    // more explicitly decide whether this needs to happen, based on family layout within a parent group.
    let midY = d3context.mean([source.y, target.y, target.y])
    const GENDER_FRACTIONAL_OFFSET = 6

    if (source.gender === 'male') {
      midY += (target.y - source.y) / GENDER_FRACTIONAL_OFFSET
    }

    return basicPathGenerator([[source.x, source.y], [source.x, midY], [target.x, midY], [target.x, target.y]])
  }

  if (edge.type === 'child') {
    const { source, target } = edge

    if (target.twinset) {
      const midX = d3context.mean(target.twinset, n => n.x)

// FIXME: iconSize isn't a real value.
      return basicPathGenerator([[source.x, source.y], [midX, source.y], [target.x, target.y - (target.size / 2)]])
    }

    return stepPathGenerator([edge.source, edge.target])
  }

  return linePathGenerator([edge.source, edge.target])
}

export function disoriented(n) { return n.x === undefined || n.y === undefined }
