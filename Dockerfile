FROM nginx

EXPOSE 80
ENV PORT 80

COPY dist /usr/share/nginx/html
COPY demo /usr/share/nginx/html

RUN ln -s /usr/share/nginx/html/fluid-pedigree.min.js /usr/share/nginx/html/fluid-pedigree.js
