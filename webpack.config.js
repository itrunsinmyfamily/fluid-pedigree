/* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */
const webpack = require('webpack')
const path = require('path')

module.exports = {
  entry: './lib/pedigree.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'fluid-pedigree.min.js',
    libraryTarget: 'var',
    library: 'FluidPedigree',
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
    }],
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      output: {
        comments: false,
      },
    }),
  ],
}
