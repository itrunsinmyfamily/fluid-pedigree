/* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')
const path = require('path')

module.exports = {
  devtool: 'eval',
  entry: './lib/pedigree.js',
  output: {
    path: path.resolve(__dirname, './.tmp'),
    filename: 'fluid-pedigree.js',
    libraryTarget: 'var',
    library: 'FluidPedigree',
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
    }],
  },
  plugins: [
    new BrowserSyncPlugin({
      host: 'localhost',
      port: 3030,
      server: { baseDir: ['.tmp', 'demo'] },
    }),
  ],
}
