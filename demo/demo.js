/* eslint-env browser */
/* global Vue, FluidPedigree */

const INVERSES = { male: 'female', female: 'male', parent: 'child', child: 'parent', spousal: 'spousal' }
const PROFILES_BASEURL = 'https://randomuser.me/api/?noinfo&nat=us,es,de,dk,fi,fr&inc=name,picture'

const EFFECTS = {
  pop: [new Audio('pop.mp3')],
  swish: [new Audio('swish.mp3')],
}

Object.keys(EFFECTS).forEach(name => {
  const group = EFFECTS[name]
  const effect = group[0]

  effect.volume = 0.1
  for (let i = 1; i < 4; i++) {
    const e = effect.cloneNode()

    e.volume = 0.1
    EFFECTS[name].push(e)
  }
})

window.demo = new Vue({
  el: '#app',
  data() {
    return {
      preset: 'samples/large.json', // 'samples/nuclear.json',
      highlight: '',
      graph: null,
      pedigree: null,
      selected: null,
      hovered: null,
      context: null,
      randomizedProfiles: true,
      soundEffects: true,
      autoZoomFit: true,
      maxStep: 0,
      newPatientIndex: 1,
      options: {
        pedigree: {
          showLinks: false,
        },
        templates: {
          patient: document.getElementById('patient-template').innerHTML,
          familyVertex: document.getElementById('family-vertex-template').innerHTML,
          defs: document.getElementById('pedigree-defs-template').innerHTML,
        },
      },
    }
  },
  watch: {
    preset(preset) {
      return this.loadPreset(preset)
    },
    randomizedProfiles() {
      this.persistState()
      this.loadPreset(this.preset)
    },
    soundEffects() {
      this.persistState()
    },
    autoZoomFit() {
      this.persistState()
      if (this.autoZoomFit) {
        this.pedigree.zoomFit(1000)
      }
    },
    'options.pedigree.showLinks'() {
      this.persistState()
      this.pedigree.options(this.options)
    },
    maxStep() {
      this.pedigree.graph.stepTo(parseInt(this.maxStep, 10))
      this.pedigree.graph.changed = true
      this.pedigree.render()
    },
    highlight(highlight) {
      const patients = []
      let subset

      Object.keys(this.graph).forEach(id => patients.push(this.graph[id]))

      switch (highlight) {
        case 'males':
          subset = patients.filter(n => n.gender === 'male').map(n => n.id)
          break
        case 'females':
          subset = patients.filter(n => n.gender === 'female').map(n => n.id)
          break
        case 'children':
          subset = patients.filter(n => n.age && n.age < 18).map(n => n.id)
          break
        case 'adults':
          subset = patients.filter(n => n.age && n.age >= 18).map(n => n.id)
          break
        case '':
        default:
          subset = undefined
      }

      this.pedigree.highlight(subset)
    },
  },
  computed: {
    canRemoveSelected() {
      return this.selected && !this.selected.proband && (
        !this.selected.relations.parent.length ||
        !this.selected.relations.child.length)
    },
  },
  methods: {
    persistState() {
      localStorage.setItem('options', JSON.stringify({
        randomizedProfiles: this.randomizedProfiles,
        soundEffects: this.soundEffects,
        autoZoomFit: this.autoZoomFit,
        showLinks: this.options.pedigree.showLinks,
      }))
    },
    commitAttribute(attribute) {
      const patientId = this.selected.id
      const value = this.selected[attribute]

      this.graph[patientId][attribute] = value
      this.pedigree.data(this.graph)
    },
    playEffect(name, numRepeat = 0) {
      if (!this.soundEffects) {
        return
      }

      const group = EFFECTS[name]
      const effect = group.shift()

      effect.play()

      // Cycle effects to allow for parallel playback
      group.push(effect)

      if (numRepeat > 0) {
        const delay = 50 + (Math.random() * 200)

        setTimeout(() => this.playEffect(name, numRepeat - 1), delay)
      }
    },
    loadPreset(url = this.preset) {
      console.info('Loading preset', url, this.randomizedProfiles)

      this.hovered = null
      this.selected = null
      this.context = null
      this.maxStep = 0
      this.highlight = ''

      this.pedigree.init()

      return fetch(url)
        .then(r => r.json())
        .then(graph => {
          if (!this.randomizedProfiles) {
            this.graph = graph
            this.pedigree.data(graph)
            setTimeout(() => this.playEffect('pop', 4), 250)
            return null
          }

          const patients = Object.keys(graph).map(id => graph[id])

          return this.randomize(patients).then(() => {
            this.graph = graph
            this.pedigree.data(graph)
            setTimeout(() => this.playEffect('pop', 4), 250)
          })
        })
    },
    addRelation(context, relation) {
      const patient = this.graph[context.id]
      const inverseRelationType = INVERSES[relation.type]
      const id = 'new' + (this.newPatientIndex++)

      this.graph[id] = {
        id,
        gender: relation.gender,
        relations: {
          [inverseRelationType]: [context.id],
        },
      }

      patient.relations[relation.type] = patient.relations[relation.type] || []
      patient.relations[relation.type].push(id)

      if (relation.coparent) {
        const coparent = this.graph[relation.coparent.id]

        coparent.relations.child = coparent.relations.child || []
        coparent.relations.child.push(id)
        this.graph[id].relations.parent.push(coparent.id)
      }

      const performUpdate = () =>
        Promise.resolve().then(() => {
          this.pedigree.data(this.graph)
          this.playEffect('pop')
          return
        })

      if (!this.randomizedProfiles) {
        return performUpdate()
      }

      return this.randomize([this.graph[id]]).then(performUpdate)
    },
    removePatient(removal) {
      if (removal.proband) {
        alert('Stubbornly refusing to remove proband from graph.')
        return
      }
      const removalId = removal.id

      Object.keys(this.graph).forEach(patientId => {
        const patient = this.graph[patientId]

        Object.keys(patient.relations).forEach(relationType => {
          patient.relations[relationType] = patient.relations[relationType].filter(id => id != removalId)
        })
      })

      delete this.graph[removalId]
      this.pedigree.data(this.graph)
      this.playEffect('swish')
    },
    randomize(patients) {
      const males = patients.filter(p => p.gender === 'male')
      const females = patients.filter(p => p.gender === 'female')
      const twinAges = {}

      const process = (targets, data) => {
        targets.forEach((t, i) => {
          const d = data[i]

          t.firstName = d.name.first.charAt(0).toUpperCase() + d.name.first.substr(1)
          t.lastName = d.name.last.charAt(0).toUpperCase() + d.name.last.substr(1)
          if ((Math.random() * 10) > 6) {
            t.photo = d.picture.large
          }

          if (t.twinId) {
            t.age = twinAges[t.twinId] = twinAges[t.twinId] || Math.floor(Math.random() * 95) + 5
          } else {
            t.age = Math.floor(Math.random() * 95) + 5
          }
          t.deceased = (Math.random() * 10) < 1
        })
      }

      return Promise.all([
        fetch(`${PROFILES_BASEURL}&gender=male&results=${males.length}`),
        fetch(`${PROFILES_BASEURL}&gender=female&results=${females.length}`),
      ])
      .then(results => Promise.all(results.map(r => r.json())))
      .then(results => {
        process(males, results[0].results)
        process(females, results[1].results)
      })
    },
    hover(node, e) {
      e.preventDefault()
      e.stopPropagation()

      if (!node) {
        this.hovered = null
        return
      }

      const hovered = this.graph[node.id]
      const el = e.target.nearestViewportElement

      if (!el) {
        return
      }

      const pos = el.getBoundingClientRect()

      const container = this.pedigree.svg.node().getBoundingClientRect()
      const popup = this.$refs.patientPopup
      const targetPageX = (e.clientX - e.offsetX) + (pos.left - container.left)
      const targetPageY = (e.clientY - e.offsetY) + (pos.top - container.top)

      popup.style.left = (targetPageX + pos.width) + 'px'
      popup.style.top = (targetPageY - 80) + 'px'

      this.hovered = hovered
    },
  },

  created() {
    const options = JSON.parse(localStorage.getItem('options'))

    if (options) {
      this.randomizedProfiles = options.randomizedProfiles
      this.soundEffects = options.soundEffects
      this.autoZoomFit = options.autoZoomFit
      this.options.pedigree.showLinks = options.showLinks
    }
  },

  mounted() {
    this.pedigree = new FluidPedigree.default(this.$refs.chart, this.options)

    this.options = this.pedigree.options()

    this.loadPreset(this.preset)

    this.pedigree
      .on('selected', patient => (this.selected = patient ? this.graph[patient.id] : null))
      .on('hovered', (node, e) => this.hover(node, e))
      .on('dragged', (node, e) => this.hover(node, e.sourceEvent))
      .on('changed', () => this.autoZoomFit && setTimeout(() => this.pedigree.zoomFit(1000), 250))
      .on('contextmenu', (patient, e) => {
        e.preventDefault()
        e.stopPropagation()

        this.$refs.contextmenu.style.top = e.clientY + 'px'
        this.$refs.contextmenu.style.left = e.clientX + 'px'
        this.context = patient
      })
      .on('data', () => {
        if (this.pedigree.selected) {
          this.selected = this.graph[this.pedigree.selected]
        } else {
          this.selected = null
        }
      })
  },
})
