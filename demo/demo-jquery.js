/* eslint-env jquery, browser */
/* global Pedigree */
jQuery($ => {
  const demo = window.demo = new Pedigree($('#demo')[0], {
    templates: {
      patient: $('#patient').html(),
      familyVertex: $('#family-vertex').html(),
      defs: $('#pedigree-defs').html(),
    },
  })

  loadPreset('samples/nuclear.json')

  $('#presets').change(e => {
    e.preventDefault()
    loadPreset($('#presets').val())
  })

  demo.on('selected', patient => {
    console.log('Patient Selected:', patient)
    const $form = $('form#patient-controls')

    if (!patient) {
      $form.hide()
      return
    }

    $form.show()
    $form.find('#firstName').val(patient.attributes.firstName)
    $form.find('#lastName').val(patient.attributes.lastName)
    $form.find('[name="gender"]').val([patient.attributes.gender])
    $form.find('#age').val(patient.attributes.age)
  })

  demo.container.on('click', () => $('#contextmenu').hide())

  demo.on('contextmenu', (patient, e) => {
    e.preventDefault()
    e.stopPropagation()

    const $menu = $('#contextmenu')

    $menu.data('patientId', patient.id)
    $menu.show()
    $menu.offset({ top: e.pageY, left: e.pageX })
  })

  $('#contextmenu li').click(function (e) {
    e.preventDefault()
    const patientId = $('#contextmenu').data('patientId')
    const action = $(this).data('action')

    if (action === 'add') {
      addRelation(demo, patient.id, $(this).data('relation'), $(this).data('gender'))
    }
    if (action === 'remove') {
      removePatient(demo, patient)
    }

    $('#contextmenu').hide()
  })

  $('body').contextmenu(() => $('#contextmenu').hide())

  $('#forcelinks').change(e => {
    e.preventDefault()
    const options = demo.options()

    options.pedigree.showLinks = this.checked
    demo.options(options)
  })

  $('#patient-controls input[name="gender"], #age').change(updatePatient)
  $('#firstName, #lastName').keyup(updatePatient)

  function updatePatient(e) {
    e.preventDefault()
    const $form = $('form#patient-controls')
    const data = demo.data()
    const patient = data[demo.selected]

    patient.attributes.firstName = $form.find('#firstName').val()
    patient.attributes.lastName = $form.find('#lastName').val()
    patient.attributes.gender = $form.find('[name="gender"]:checked').val()
    patient.attributes.age = $form.find('#age').val()

    demo.data(data)
  }
})

function loadPreset(url) {
  if (!url) {
    return window.demo.data()
  }

  console.log('Loading preset', url)

  $('form#patient-controls').hide()
  $('#presets').val('')

  window.demo.selected = null
  window.demo.highlighted = []

  return fetch(url).then(r => r.json())
  .then(graph => window.demo.data(graph))
}

function addRelation(chart, patientId, relationType, gender) {
  const INVERSES = {
    male: 'female',
    female: 'male',
    parent: 'child',
    child: 'parent',
    spousal: 'spousal',
  }

  const data = chart.data()
  const patient = data[patientId]
  const inverseRelationType = INVERSES[relationType]
  const id = 'new' + Object.keys(data).length
  const relative = {
    id,
    attributes: {
      firstName: '',
      lastName: '',
      age: undefined,
      gender,
    },
    relations: {
      [inverseRelationType]: [patient.id],
    },
  }

  patient.relations[relationType] = patient.relations[relationType] || []
  patient.relations[relationType].push(relative.id)

  data[id] = relative

  chart.data(data)
}
